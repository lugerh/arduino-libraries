#ifndef RUNEVERY_h
#define RUNEVERY_h

#include <arduino.h>

class runEvery
{
private:
	unsigned long prevMillis=0;
	unsigned long timeToRepeat=0;
	bool state=false;
	bool useState=false;
	void (*inputThing)(void);
	void (*inputThing2)(bool);
public:
	runEvery(void (*_inputThing)(), unsigned long _timeToRepeat)
	{
		inputThing=_inputThing;
		timeToRepeat=_timeToRepeat;
		useState=false;
	}

	runEvery(void (*_inputThing)(bool), unsigned long _timeToRepeat)
	{
		inputThing2=_inputThing;
		timeToRepeat=_timeToRepeat;
		useState=true;
	}

	void changeInterval(unsigned long _timeToRepeat)
	{
		timeToRepeat=_timeToRepeat;
	}

	void zero()
	{
		zero(millis());
	}

	void zero(unsigned long _prevMillis)
	{
		prevMillis=_prevMillis;
	}

	void update()
	{
		update(millis());
	}

	void update(unsigned long _millis)
	{
		if (_millis - prevMillis >= timeToRepeat)
		{
			state=!state;
			prevMillis=_millis;
			if (useState)
				(*inputThing2)(state);
			else
				(*inputThing)();
		}
	}
};
#endif
