/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly without blocking the program loop.

	modified 7 Jul 2020
	by Lugerh (luige.rh@gmail.com)
*/

#include "runEvery.h"

#define INTERVAL_LED	1000  // Set an interval of execution (ms o us)

runEvery	ledBlink(&ToggleLed ,INTERVAL_LED);		// instance de object with the function to run

void setup()
{
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
	// the update function runs over and over again forever
	// put this in your update and don't use delays and your function will run without delay,
	// or that should happen ...

  // This calls the function when the time has elapsed
  ledBlink.update();		// internally use millis()...
  // ledBlink.update(micros());  		 // You can use micros() by passing it as a parameter
}

// This function will be executed every time the interval time passes
// (approximately, it is not precise at all)
void ToggleLed()
{
	// This function will be executed every time the interval time passes
	//(approximately, it is not precise at all)
	// It is important that the function has no return or input parameters.

	// Create a static variable to toggle the led.
	static bool _ledState=false;

	// Switch to the opposite state
	_ledState=!_ledState;

	// Finally, this turns a led on and off, or whatever you want, be creative!
	digitalWrite(LED_BUILTIN, _ledState);
}
